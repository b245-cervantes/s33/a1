// console.log("Hello World")

// create fetch request using get method that will retrieve all the to do 
//list items from JSON placeholder API

fetch("https://jsonplaceholder.typicode.com/todos", {
    method : "GET",
})
.then(res => res.json())
.then(result => console.log(result))

//Using the data retrieved, create an array using the map method to
//return just the title of every item and print the result in the console.

fetch("https://jsonplaceholder.typicode.com/todos", {
    method: "GET"
})
.then(res => {
    return res.json()
})
.then(result => {
    let title = result.map(el => el.title)
    console.log(title)
})

//Create a fetch request using the GET method that will retrieve a single
//to do list item from JSON placeholder API

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method : "GET"
})
.then(res => {
    return res.json()
})
.then(result => {
    console.log(result)
})

//Using the data retrieved, print a message in the console that will provid
// the title and status of the to do list item

fetch("https://jsonplaceholder.typicode.com/todos/1", {
    method: "GET"
})
.then(res => {
    return res.json()
})
.then(result => {
    console.log(`The item ${result.title} on the list has a status of ${result.completed}`);
})

//Create fetch request using the POST method that will create a to do list
// using the JSON placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos", {
    method : "POST",
    headers : {
        "Content-Type":"application/json"
    },
    body: JSON.stringify({
       completed: false,
       title: "Created To Do List Item",
       userId: 1
    })
})
.then(res => {
    return res.json()
})
.then(result => {
    console.log(result)
})

//Create a fetch request using the PUT method that will update a to do
//list item using the JSON placeholder 

fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PUT",
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        completed: false,
        title: "Updated To Do List Item",
        userID: 1
    })
})
.then(res => {
    return res.json()
})
.then(result => {
    // console.log(result)
})

//UPDATE a to do list item by changing the data structure to contain
// following properties
/**
 *  a title
 *  b description
 *  c status
 *  d date compeleted
 *  e user ID
 */

fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PUT",
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        completed: "Pending",
        description: "To update the my to do list with a different data structures",
        status: "Pending",
        title: "Updated To Do List Item",
        userID: 1
    })
})
.then(res => {
    return res.json()
})
.then(result => {
    console.log(result)
})

//Create a fetch request using the PATCH method that will update a to do list
// using the JSON placeholder API

fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PATCH",
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        completed: false,
        description: "To update the my to do list with a different data structures",
        status: "Pending",
        title: "delectus aut autem",
        userId: 1 
    })
})
.then(res => {
    return res.json()
})
.then(result => {
    // console.log(result);
})

// AND
// Update a to do list item by changing the status to complete and add a date 
//when the status is change

fetch("https://jsonplaceholder.typicode.com/todos/5", {
    method: "PATCH",
    headers: {
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        completed: false,
        dateCompleted: "07/09/21",
        status: "Complete",
        title: "delectus aut autem",
        userId: 1 
    })
})
.then(res => {
    return res.json()
})
.then(result => {
    console.log(result);
})

//Create a fetch request using the delete method that will delete an
//item using the JSON placeholder API.

fetch("https://jsonplaceholder.typicode.com/todos/5",{
    method : "DELETE"
})

.then(res => {
    return res.json()
})
.then(result => {
    console.log(result);
})

